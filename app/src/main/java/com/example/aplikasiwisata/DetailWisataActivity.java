package com.example.aplikasiwisata;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.List;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class DetailWisataActivity extends AppCompatActivity {

    TextView tvname, tvweb, tvkontak, tvdeskripsi, tvlokasi, tvjarak;
    ImageView ivrating;
    ArrayList<List> reviewList;
    DatabaseReference databaseReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detail_wisata_activity);

//        tvname = findViewById(R.id.namalokasi);
//        tvjarak = findViewById(R.id.jarak);
//        tvweb = findViewById(R.id.website);
//        tvkontak = findViewById(R.id.nomortelp);
        tvdeskripsi = findViewById(R.id.isideskripsi);
//        tvlokasi = findViewById(R.id.isilokasi);


//        tvweb.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent webIntent = new Intent(Intent.ACTION_VIEW);
//                webIntent.setData(Uri.parse("https://florawisatasanterra.com"));
//                startActivity(webIntent);
//            }
//        });
//
//        tvkontak.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent callIntent = new Intent(Intent.ACTION_DIAL);
//                callIntent.setData(Uri.parse("tel:+6285232351435"));
//                startActivity(callIntent);
//            }
//        });



        getData();
    }
    private void getData() {

        databaseReference = FirebaseDatabase.getInstance().getReference("/Kota/Malang/list/San Terra");

        databaseReference.addValueEventListener(new ValueEventListener() {

            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                Detail detailWisata = snapshot.getValue(Detail.class);

//                tvname.setText(detailWisata.getId());
//                tvjarak.setText(detailWisata.getJarak());
//                tvweb.setText(detailWisata.getWebsite());
//                tvkontak.setText(detailWisata.getKontak());
                tvdeskripsi.setText(detailWisata.getDeskripsi());
//                tvlokasi.setText(detailWisata.getLokasi());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });


//        reference.child(id).get().addOnCompleteListener(new OnCompleteListener<DataSnapshot>() {
//            @Override
//            public void onComplete(@NonNull Task<DataSnapshot> task) {
//
//                if (task.isSuccessful()) {
//
//                    if (task.getResult().exists()) {
//
//                        Toast.makeText(DetailWisataActivity.this, "Successfully Read", Toast.LENGTH_SHORT).show();
//                        DataSnapshot dataSnapshot = task.getResult();
//                        String namaWisata = String.valueOf(dataSnapshot.child("id").getValue());
//                        String websiteWisata = String.valueOf(dataSnapshot.child("website").getValue());
//                        String kontakWisata = String.valueOf(dataSnapshot.child("kontak").getValue());
//                        String deskripsiWisata = String.valueOf(dataSnapshot.child("deskripsi").getValue());
//                        String lokasiWisata = String.valueOf(dataSnapshot.child("lokasi").getValue());
//
//                        tvname.setText(namaWisata);
//                        tvweb.setText(websiteWisata);
//                        tvkontak.setText(kontakWisata);
//                        tvdeskripsi.setText(deskripsiWisata);
//                        tvlokasi.setText(lokasiWisata);
//
//                    }
//                }
//            }
//        });
    }
}