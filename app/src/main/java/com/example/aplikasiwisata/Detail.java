package com.example.aplikasiwisata;

public class Detail {
    private String id;
    private String website;
    private String kontak;
    private String deskripsi;
    private String lokasi;
    private String jarak;

    public Detail() {
    }

    public Detail(String id, String website, String kontak, String deskripsi, String lokasi, String jarak) {
        this.id = id;
        this.website = website;
        this.kontak = kontak;
        this.deskripsi = deskripsi;
        this.lokasi = lokasi;
        this.jarak = jarak;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getKontak() {
        return kontak;
    }

    public void setKontak(String kontak) {
        this.kontak = kontak;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public String getLokasi() {
        return lokasi;
    }

    public void setLokasi(String lokasi) {
        this.lokasi = lokasi;
    }

    public String getJarak() {
        return jarak;
    }

    public void setJarak(String jarak) {
        this.jarak = jarak;
    }
}
